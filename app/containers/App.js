import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Slider from 'react-slick';
import { Link } from 'react-router';

import LandingSlide from '../components/LandingSlide/index';
import * as TodoActions from '../actions/todos';


@connect(
  state => ({
    todos: state.todos
  }),
  dispatch => ({
    actions: bindActionCreators(TodoActions, dispatch)
  })
)
export default class App extends Component {

  static propTypes = {
    todos: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired
  };

  render() {
    const { todos, actions } = this.props;
    const settings = {
      dots: true,
      infinite: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    return (
      <div>
        <Slider {...settings}>
          <LandingSlide title="Store" image="./images/wallet-eql.png" desc="Store all your tokens in one simple to use wallet." />
          <LandingSlide title="Send" image="./images/eql-white-2.png" desc="Send any token without the need for MyEtherWallet." />
          <LandingSlide title="Receive" image="./images/eql-white-2.png" desc="Receive Ethereum and any ERC20 token." />
        </Slider>
        <div className="pagination">
          <Link to="/">
            <span className="skip-tutorial">Skip</span>
          </Link>
        </div>
      </div>
    );
  }
}
