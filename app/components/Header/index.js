import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';

export default class Header extends Component {

  static propTypes = {
    type: PropTypes.string.isRequired,
  };

  render() {
    const { type } = this.props;
    return (
      <div>
        <div className="wave -one" />
        <div className="wave -two" />
        <div className="wave -three" />
        <div className="imagery-container">
          <img className="nav-icon" src="./images/eql-white-2.png" role="presentation" />
          <i className="fas fa-ellipsis-v" />
        </div>
        { type !== 'confirm' && (
        <div className="balance-section">
          <span>Total Balance</span>
          <div className="balance">$129,000.00</div>
          <div className="balance balance-eth">
            11.22 ETH
          </div>
          { type !== 'send' && (
            <span className="percent-up">+120%</span>
          )}
        </div>
        )}
        { type === 'confirm' && (
        <div className="balance-section">
          <div className="balance-section">
            <div className="balance balance-no-logo">Confirm
            </div>
            <div className="balance balance-eth">
              Review your transaction.
            </div>
          </div>
        </div>
        )}
      </div>
    );
  }
}
