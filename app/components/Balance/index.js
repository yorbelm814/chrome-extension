import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import style from './style.css';

import BalanceItem from '../BalanceItem/index';

export default class Balance extends Component {

  static propTypes = {
  };

  render() {
    return (
      <div className="token-list">
        <ul data-simplebar>
          <li>
            <BalanceItem tokenType="eth" image="./images/ether.png" tokenCount="200" balance="5,000" percent="20" />
          </li>
          <li>
            <BalanceItem tokenType="eql" image="./images/eql-white-2.png" tokenCount="1200" balance="400" percent="-20" />
          </li>
          <li>
            <BalanceItem tokenType="salt" image="./images/salt.png" tokenCount="2200" balance="15,000" percent="10" />
          </li>
          <li>
            <BalanceItem tokenType="gnt" image="./images/gnt.svg" tokenCount="2200" balance="15,000" percent="10" />
          </li>
          <li>
            <BalanceItem tokenType="snt" image="./images/snt.png" tokenCount="2200" balance="15,000" percent="10" />
          </li>
          <li>
            <BalanceItem tokenType="bnty" image="./images/bnty.png" tokenCount="2200" balance="15,000" percent="10" />
          </li>
          <li>
            <BalanceItem tokenType="dnt" image="./images/dnt.png" tokenCount="2200" balance="15,000" percent="10" />
          </li>
        </ul>
      </div>
    );
  }
}
