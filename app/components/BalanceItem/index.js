import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';

export default class BalanceItem extends Component {

  static propTypes = {
    tokenType: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    tokenCount: PropTypes.string.isRequired,
    balance: PropTypes.string.isRequired,
    percent: PropTypes.string.isRequired
  };

  render() {
    const { tokenType, image, tokenCount, balance, percent } = this.props;
    const changeClass = classnames({
      'change': true,
      'neg-change': parseFloat(percent) < 0
    });

    const eqlBgClass = classnames({
      'eql-logo': tokenType === 'eql',
    });

    return (
      <div>
        <div className="image-container"><img className={eqlBgClass} src={image} role="presentation" /></div>
        <div className="token-info">
          <div className="token-name">ETH</div>
          <div className="token-amount"><span>{tokenCount}</span></div>
        </div>
        <div className="change-container">
          <div className="currency-value">$ {balance}</div>
          <div className={changeClass}>{percent}% </div>
        </div>
        <button tabIndex="0"><i className="fas fa-arrow-right" /></button>
      </div>
    );
  }
}
