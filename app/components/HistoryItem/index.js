import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';

export default class HistoryItem extends Component {

  static propTypes = {
    tokenType: PropTypes.string.isRequired,
    transactionId: PropTypes.string.isRequired,
    transferAmount: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
  };

  render() {
    const { tokenType, transactionId, transferAmount, date } = this.props;
    const transferAmoutValue = parseFloat(transferAmount);

    const txClass = classnames({
      'tx-amount': true,
      'tx-in': transferAmoutValue > 0,
      'tx-out': transferAmoutValue < 0,
      'tx-fail': transferAmoutValue === 0
    });

    return (
      <div>
        { transferAmoutValue > 0 && (
          <div className="direction direction-in">
            <i className="fas fa-plus" />
          </div>
        )}

        { transferAmoutValue < 0 && (
          <div className="direction direction-out">
            <i className="fas fa-minus" />
          </div>
        )}

        { transferAmoutValue === 0 && (
          <div className="direction">
            <i className="fas fa-exclamation" />
            <div className="triangle" />
          </div>
        )}
        <div className="token-info">
          <div className="token-name">{transactionId}</div>
          <div className="transfer-date"><span>{date}</span> hours ago</div>
          <div className="usd-value" />
        </div>
        <div className={txClass}>{transferAmount} <span className="symbol">{tokenType}</span></div>
      </div>
    );
  }
}
