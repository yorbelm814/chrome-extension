import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import { Link } from 'react-router';
import TextField from 'material-ui/TextField';

import style from './style.css';

import Header from '../Header/index';
import Footer from '../Footer/index';

export default class Confirm extends Component {

  static propTypes = {
  };

  constructor(props, context) {
    super(props);
  }

  state = {
    sliderValue: 10
  };

  handleChange(evt) {
    this.setState({
      sliderValue: evt.target.value
    });
  }

  render() {
    return (
      <div className="account-wallet chrome-extension-container receipt-screen">
        <Header type="confirm" />
        <div className="receipt-container">
          <h4 className="receipt-amount">200 <span>EQL</span></h4>
          <div className="receipt-info">
            <div>To</div>
            <div>0x80c63.....ada7df<br />
            </div>
          </div>
          <div className="receipt-info receipt-fee-info">
            <div>Gas Fee</div>
            <div>0.19 USD<br />
             <span>0.000273 ETH</span>
            </div>
          </div>
          <div className="receipt-info">
            <div>Burn Fee</div>
            <div>2 EQL<br />
            </div>
          </div>
          <div className="receipt-info">
            <div>Total<br />
              <span>Gas <br /> Burn</span>
            </div>
            <div>200 EQL <br />
              <span>+ 0.19 USD Gas</span><br />
              <span>+ 2 EQL Burn</span>
            </div>
          </div>
          <div className="receipt-confirm">
            <Link to="/pay">
              <div className="general-btn cancel-btn">
                <span>Cancel</span>
              </div>
            </Link>
            <Link to="/dashboard">
              <div className="general-btn confirm-btn">
                <span>Confirm</span>
                <div className="wave -button" />
              </div>
            </Link>
            <div className="transaction-warning">
              <span>Warning: Blockchain transactions are irreversible, double check your transaction details before sending.</span>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
} 
