import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import style from './style.css';

import Header from '../Header/index';
import Footer from '../Footer/index';
import Balance from '../Balance/index';
import History from '../History/index';

export default class Dashboard extends Component {

  static propTypes = {
  };

  state = {
    currentTab: 'balance'
  }

  changeTab(event, value) {
    this.setState({ currentTab: value });
  }

  tabClass(value) {
    return classnames({
      'unselected-right': this.state.currentTab !== value
    });
  }
  render() {
    return (
      <div className="account-wallet chrome-extension-container">
        <Header />
        <div className="wallet-address-container">
          <div className="wallet-address">0x1db834aD5CA73782Dcb3d88974438e8fCDc9f485</div>
          <i className="far fa-copy" />
        </div>
        <div className="tabs">
          <div className={this.tabClass('balance')} onClick={event => this.changeTab(event, 'balance')}>
            Balances
          </div>
          <div className={this.tabClass('history')} onClick={event => this.changeTab(event, 'history')}>
            History
          </div>
        </div>
        <div>
          { this.state.currentTab === 'balance' ? (
            <Balance />
          ) : (
            <History />
          )}
        </div>
        <Footer />
      </div>
    );
  }
}
