import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import style from './style.css';

import HistoryItem from '../HistoryItem/index';

export default class Balance extends Component {

  static propTypes = {
  };

  render() {
    return (
      <div className="token-list">
        <ul data-simplebar>
          <li>
            <HistoryItem tokenType="eth" transactionId="0x345f1c..1233" transferAmount="200" date="2" />
          </li>
          <li>
            <HistoryItem tokenType="eql" transactionId="0x345f1c..1234" transferAmount="0" date="2" />
          </li>
          <li>
            <HistoryItem tokenType="eth" transactionId="0x345f1c..2343" transferAmount="2,000" date="3" />
          </li>
          <li>
            <HistoryItem tokenType="eth" transactionId="0x345f1c..2314" transferAmount="-1,200" date="3" />
          </li>
          <li>
            <HistoryItem tokenType="eth" transactionId="0x345f1c..2314" transferAmount="2,000" date="3" />
          </li>
          <li>
            <HistoryItem tokenType="eth" transactionId="0x345f1c..2314" transferAmount="210" date="4" />
          </li>
          <li>
            <HistoryItem tokenType="eth" transactionId="0x345f1c..2314" transferAmount="0" date="4" />
          </li>
        </ul>
      </div>
    );
  }
}
