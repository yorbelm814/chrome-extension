import React, { PropTypes, Component } from 'react';
import { Link } from 'react-router';
import TextField from 'material-ui/TextField';
import './style.css';

export default class Register extends Component {

  static propTypes = {
  };

  render() {
    return (
      <div className="chrome-extension-container seed-backup-screen">
        <div className="wave -one" />
        <div className="wave -two" />
        <div className="wave -three" />
        <div className="imagery-container">
          <img className="nav-icon" src="./images/eql-white-2.png" role="presentation" />
          <i className="fas fa-ellipsis-v" />
        </div>
        <div className="balance-section">
          <div className="balance balance-no-logo">Seed Backup
          </div>
          <div className="balance balance-eth">
            Stay safe and backup your wallet
          </div>
        </div>

        <div className="seed-container">
          <div className="seed-generation-container">
            <input type="text" className="seed-generation" placeholder="Type 12 words here" />
            <div className="backup-suggestion">
              <span>These 12 words are the <strong>ONLY</strong> way to restore your EQUAL wallet, save them someone where safe! <br /></span>
            </div>
            <div className="general-btn download-seed">
              <span>Download Seed Backup <i className="fas fa-arrow-down" /></span>
              <div className="wave -button" />
            </div>
          </div>


          <div className="seed-confirm">
            <TextField
              id="password"
              label="Password"
              fullWidth="true"
              margin="normal"
            />
            <TextField
              id="confirm_password"
              label="Confirm password"
              fullWidth="true"
              margin="normal"
            />
            <Link to="/login">
              <div className="general-btn">
                <span>Create account</span>
                <div className="wave -button" />
              </div>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}
