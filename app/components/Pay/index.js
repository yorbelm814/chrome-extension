import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import { Link } from 'react-router';
import TextField from 'material-ui/TextField';

import style from './style.css';

import Header from '../Header/index';
import Footer from '../Footer/index';

export default class Dashboard extends Component {

  static propTypes = {
  };

  constructor(props, context) {
    super(props);
  }

  state = {
    sliderValue: 10
  };

  handleChange(evt) {
    this.setState({
      sliderValue: evt.target.value
    });
  }

  render() {
    return (
      <div className="account-wallet chrome-extension-container">
        <Header type="send" />
        <div className="send-container">
          <TextField
            id="recipient"
            className="pay_text"
            label="Recipient"
            fullWidth="true"
            margin="normal"
          />
          <TextField
            id="transferAmount"
            className="pay_text"
            label="Amount"
            fullWidth="true"
            margin="normal"
          />
          <div className="tx-speed-container">
            <div className="speed-title">Transaction Speed</div>
            <input className="mdl-slider mdl-js-slider" type="range" value={this.state.sliderValue} onChange={event => this.handleChange(event)} min="0" max="100" tabIndex="0" />
            <div className="transaction-info">
              <div>Slow</div>
              <div>{this.state.sliderValue} ETH</div>
              <div>Fast</div>
            </div>
          </div>
          <div className="send-btn-container">
            <div className="fee-summary">
              <div>
                <span className="max-fee">Max Fee</span>
                <span className="fee-eth">0.000375</span>ETH<br />
                <span className="fee-eth">0</span>EQL
              </div>
              <span className="usd-fee">0.25</span> USD
            </div>
            <Link to="/confirm-payment">
              <div className="general-btn">
                <span>Send EQL</span>
                <div className="wave -button" />
              </div>
            </Link>
            <div className="transaction-warning">
              <span>Warning: Blockchain transactions are irreversible, double check your transaction details before sending.</span>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
