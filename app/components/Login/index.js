import React, { PropTypes, Component } from 'react';
import { Link } from 'react-router';
import TextField from 'material-ui/TextField';
import './style.css';

export default class Login extends Component {

  static propTypes = {
  };

  render() {
    return (
      <div className="chrome-extension-container tutorial">
        <div className="wave -one" />
        <div className="wave -two" />
        <div className="wave -three" />
        <div className="title">
          <span className="app-title">EQUAL</span>
          <div className="powered-by">
            <span>Powered by the <br /> EQL Network Token </span>
          </div>
        </div>
        <div className="imagery-container">
          <img className="float float-0" src="./images/eql-white-2.png" role="presentation" />
          <img className="float floating-logos float-1" src="./images/ethereum-logo.png" role="presentation" />
          <img className="float floating-logos float-2" src="./images/0x-logo.png" role="presentation" />
          <img className="float floating-logos float-3" src="./images/augur-logo.png" role="presentation" />
          <img className="float floating-logos float-4" src="./images/bnb-logo.png" role="presentation" />
          <img className="float floating-logos float-5" src="./images/status-logo.png" role="presentation" />
        </div>
        <div className="logo" />
        <div className="description login-info">
          <div className="login-input-btn-container">
            <TextField
              id="password"
              className="password-text"
              label="Password"
              fullWidth="true"
              margin="normal"
            />

            <Link to="/dashboard">
              <div className="general-btn">
                <span>Log In</span>
                <div className="wave -button" />
              </div>
            </Link>
          </div>
          <Link to="/">
            <a>Restore EQUAL from seed phrase</a>
          </Link>
        </div>
      </div>
    );
  }
}
